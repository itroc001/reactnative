/**{}
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';
import React from 'React';
import ReactNative from 'react-native';
import Login from './Login';
import AuthService from './AuthService';

var buffer = require('buffer');
var {
    AppRegistry,
    Text,
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight,
    Component,
    ActivityIndicator
} = ReactNative;


// class GithubBrowser extends React.Component {
var GithubBrowser = React.createClass({
    render: function() {
        if (this.state.isLoggedIn) {
            return ( 
              <View style={styles.container} >
              <Text style= {styles.welcome}>Hello World</Text>
              </View>
            );
        } 
        else {
            return ( 
              <Login onLogin = {this.onLogin}/>
            );
        }
},
onLogin: function() {
    console.log('succesfully logged in');
},
getInitialState: function() {
    return {
        isLoggedIn: false
    }
}

});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


AppRegistry.registerComponent('GithubBrowser', () => GithubBrowser);