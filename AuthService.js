var buffer = require('buffer');
var React = require('react');


class AuthService {
	login(credentials, cb){


	var b = new buffer.Buffer(credentials.username + 
    		':' + credentials.password).toString();
		console.log("b",b);

    	var encodedAuth = b.toString('base64');
    	

         fetch('https://api.github.com/user',{
    		headers:{
    			'Authorization': 'Basic ' + encodedAuth 
    		}
    	})

    	.then((response) => {
    		if(response.status >= 200 && response.status < 300 ){
    			return response.json();
    			console.log("response",response);
    		}
    			throw {
    			badCredentials: response.status == 401,
    			unknownError: response.status != 401
    		}
    	})

    	.then((response)=>{
    		return response.json();
    		console.log("response", response);
    	})
    	
    	.then((results)=>{
    		AsyncStorage.multiSet([
    			['auth',encodedAuth],
    			['user',JSON.stringify(results)]
    			], (err)=>{
    				if(err){
    					throw err;
    				}
    				return cb({success:true});
    				console.log("cb",cb);
    			})
    	})
    	.catch((err) => {
    		return cb(err);
    	})

	}
}
module.exports = new AuthService();