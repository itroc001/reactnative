'use strict';

var ReactNative = require('react-native');
var React = require('React');
var buffer = require('buffer');
import AuthService from './AuthService';

var {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Component,
  ActivityIndicator
} = ReactNative;


class Login extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			animating:false,
            username:this.props.username,
            password:this.props.password
		}
	}

    render(){

    	var errorMessage = <View />;
    	
    	if(!this.state.success && this.state.badCredentials){
    		errorMessage = <Text style={styles.error}>
    		Invalid username or password
    		</Text>;
    	}
    	if(!this.state.success && this.state.unknownError){
    		errorMessage = <Text style={styles.error}>
    		We experienced an unexpected issue
    		</Text>;
    	}
        return ( 

        	<View style = {styles.container}>
        	<Image style = {styles.logo}
        	source= {require('./images/golden-apple-logo.jpg')} />
        	<Text style={styles.heading}>
        	Ingrid's React Native App
        	</Text>
        	<TextInput 
        	onChangeText = {(text) => this.setState({username: text})}
        	style={styles.input}
        	placeholder = "Please enter username" />
        	<TextInput 
        	onChangeText = {(text) => this.setState({password: text})}
        	style={styles.input}
        	placeholder = "Please enter password" secureTextEntry="true" />
        	<TouchableHighlight 
        	onPress = {this.onLoginPressed.bind(this)} 
        	style={styles.button}>
        	<Text style = {styles.buttonText}>
        	Login
        	</Text>
        	</TouchableHighlight>
        	<TouchableHighlight 
        	style={styles.button}>
        	<Text style = {styles.buttonText}>
        	Create Account
        	</Text>
        	</TouchableHighlight>

        	{errorMessage}

        	<ActivityIndicator
        	animating = {this.state.animating}
        	size = "large"
        	style = {styles.loader}
        	 />
   	
        	</View>
        );
    }

    onLoginPressed(){
    	
    	this.setState({animating:true});
    
    var authService = require('./AuthService');

    console.log("we are here1 ",authService);

   			authService.login({
    		username:this.state.username,
    		password:this.state.password
    	},(results) => {
    		this.setState(Object.assign({
    			animating:false
    		}, results));
    		if(results.success && this.props.onLogin){
    			this.props.onLogin();
    		}
    	
    	});
   			
   			
    }
}

var styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'#F5FCFF',
		paddingTop:80,
		alignItems:'center',
		padding:10

	},
	logo: {
		width:66,
		height:55
	},
	heading:{
		fontSize:30,
		marginTop: 20,
		color:'#b76e79'
	},
	input:{
		height: 50,
		marginTop: 20,
		padding:10,
		paddingLeft:45,
		paddingRight:45,
		fontSize:18,
		borderWidth:1,
		borderColor: '#b76e79',
		
	},
	button:{
		height: 50,
		backgroundColor: '#b76e79',
		alignSelf: 'stretch',
		marginTop: 20,
		justifyContent: 'center',
		alignItems:'center',
		
	},
	
	buttonText:{
		fontSize:22,
		color:'#FFF',
		alignSelf:'center'
	},
	loader:{
		marginTop: 20
	},
	error:{
		color:'red',
		paddingTop:10
	}
});

module.exports = Login;